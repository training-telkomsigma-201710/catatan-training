# Training Microservice dengan Spring Cloud #

Studi kasus : Aplikasi Marketplace

![Struktur Aplikasi](img/microservice-marketplace.jpg)

## OAuth 2 ##

Contoh kode program:

* [Github Belajar OAuth](https://github.com/endymuhardin/belajar-springoauth2)

Terminologi dalam OAuth

![Konsep OAuth](img/01-konsep-oauth.jpg)

Berbagai Grant Type :

* Authorization Code

![Authorization Code](img/02-grant-type-authcode.jpg)

* Implicit

![Implicit](img/03-grant-type-implicit.jpg)

* User Password

![User Password](img/04-user-password.jpg)

* Client Credential

![Client Credential](img/05-client-credential.jpg)

* Device

![Device](img/06-device.jpg)
